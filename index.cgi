#!/usr/bin/python
#!C:\Progs\Python27\python.exe
from __future__ import print_function
from contextlib import closing
import os, sys
# import cgi, cgitb
import re
import mimetypes
import json

if sys.version_info < (3, 0):
    from urllib2 import urlopen
    from urllib2 import build_opener
    from urllib2 import HTTPRedirectHandler
    from urllib import addinfourl
    import urllib2 as urlerr
else:
    from urllib.request import urlopen
    from urllib.request import build_opener
    from urllib.request import HTTPRedirectHandler
    from urllib.response import addinfourl
    import urllib.error as urlerr

display_errors=False
if display_errors:
    print("Content-type: %s; charset=utf-8\n" % "text/html")    
    # cgitb.enable(1,"logs")

config={}
if os.path.isfile('config.json'):
    with open('config.json', 'r') as configfile:
        configcont=configfile.read()
        config=json.loads(configcont)
    if "emergency_stop" in config and config["emergency_stop"]==1: 
        print("Content-type: %s; charset=utf-8\n" % "text/html")
        print("<!DOCTYPE html>")
        print("<html><body>")
        print("<h2>The service is temporary desactivated for maintenance/Le service est temporairement d&eacute;sactiv&eacute; pour la maintenance</h2>")
        print("</body></html>")
        quit()
    

logdata=False
if logdata: import logging

method_pathinfo=False
scriptfilename=os.path.basename(__file__)
pathinfo=""
if "SCRIPT_URI" in os.environ:
    scripturi=os.environ["SCRIPT_URI"]
    lsu=scripturi.split('/')
    scriptname=lsu.pop()
    if scriptname == scriptfilename : method_pathinfo=True
elif "SCRIPT_URL" in os.environ:
    scripturi=os.environ["SCRIPT_URL"]
    lsu=scripturi.split('/')
    scriptname=lsu.pop()
    if scriptname == scriptfilename : method_pathinfo=True
if not method_pathinfo and "PATH_INFO" in os.environ :
    pathinfo=os.environ["PATH_INFO"]
    if pathinfo!="": method_pathinfo=True


qs=""
lqs=[]
rm=""
httphost="localhost"
if "QUERY_STRING" in os.environ:
    qs=os.environ["QUERY_STRING"]
if "REQUEST_METHOD" in os.environ:
    rm=os.environ["REQUEST_METHOD"]
protocol="http"
if "REQUEST_SCHEME" in os.environ:
    protocol=os.environ["REQUEST_SCHEME"]
if "HTTP_HOST" in os.environ:
    httphost=os.environ["HTTP_HOST"]
if logdata: logging.basicConfig(filename='otree_views.log', level=logging.INFO, format='%(asctime)s %(message)s')
data=None
if(rm=="POST"):
    data = (sys.stdin.read(int(os.environ.get('CONTENT_LENGTH', 0)))).encode('ascii')

finalpath=pathinfo if method_pathinfo else qs

port=config["port"] if "port" in config else 8000
mimetypes.add_type('application/json', '.map')
base_addr='localhost:%s/'%port+qs if not method_pathinfo else 'localhost:%s'%port+pathinfo+"?"+qs
mime=mimetypes.guess_type('http://'+base_addr)
mimetype="text/html"
if mime[0] is not None: mimetype=mime[0]
amimetypes=mimetype.split("/")
cntt=mimetype

error=False
status="200"
smurl_prefpref=""
smurl_pref=""
scripturi=""
scriptfolder="otree"
repl_suff='/?' if not method_pathinfo else '/'+scriptfilename+'/'
if "SCRIPT_NAME" in os.environ:
    areuri=os.environ["SCRIPT_NAME"].split('/')
    if len(areuri)==1 and len(os.environ["SCRIPT_NAME"].split('\\'))>1 :
        areuri=os.environ["SCRIPT_NAME"].replace('\\','/').split('/')
    scriptname=areuri.pop()
    if areuri[0]=="" : areuri.pop(0)
    scriptfolder=(str.join('/',areuri)).split('?')[0]
    while scriptfolder[-1]=="/" : scriptfolder = scriptfolder[0:-1]
    # while scriptfolder[0]=="/" : scriptfolder = scriptfolder[1:]
if qs!="" or method_pathinfo:
    lfpfinalpath=finalpath.split('?')
    lqs=lfpfinalpath[0].split('/')
    lelem=lqs.pop()
    if len(lqs)>1:
        if lqs[0]=='': lqs.pop(0)
        if lqs[0]=='static' and not method_pathinfo: repl_suff+=str.join('/',lqs)+'/'
if qs!="" or method_pathinfo:
    lqs=finalpath.split('/')
    lelem=lqs.pop()
    servername=os.environ["SERVER_NAME"] if "SERVER_NAME" in os.environ else "localhost"
    smurl_prefpref=protocol+'://'+servername+'/'+scriptfolder+repl_suff
    smurl_pref=smurl_prefpref
    if not method_pathinfo and not str.join('/',lqs) in smurl_pref: smurl_pref+=str.join('/',lqs)
info=None 

class NoRedirectHandler(HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        infourl = addinfourl(fp, headers, req.get_full_url())
        if not hasattr(infourl, 'status'): infourl.status = code
        # infourl.code = code
        return infourl
    http_error_300 = http_error_302
    http_error_301 = http_error_302
    http_error_303 = http_error_302
    http_error_307 = http_error_302
    http_error_308 = http_error_302
try:
    # with closing(urlopen('http://'+base_addr,data)) as response:
        opener = build_opener(NoRedirectHandler)
        n_http=0
        clientheaders={}
        for headername, headervalue in os.environ.items():
            if headername.startswith("HTTP_") or headername.startswith("CONTENT_"):
                cha=headername.split("_")
                if headername.startswith("HTTP_"): cha.pop(0)
                clientheaders[str.join("-",cha).title()]=headervalue
                if headername not in ["HTTP_IF_MODIFIED_SINCE"] :
                    if headername=="HTTP_REFERER": headervalue=protocol+'://'+base_addr
                    n_http+=1
                    if n_http==1: opener.addheaders = [(str.join("-",cha).title(),headervalue)]
                    else: opener.addheaders.append((str.join("-",cha).title(),headervalue))
        # opener.addheaders.append(("Referer","/SessionData/9ne8iat8/"))
        # if "HTTP_COOKIE" in os.environ: opener.addheaders.append(('Cookie', os.environ["HTTP_COOKIE"]))
        response = opener.open('http://'+base_addr,data)
        html = response.read()
        info = response.info()
        ainfo=str(info).replace('\r\n','\n').split('\n')
except urlerr.HTTPError as e:
    html = "%s %s\n%s" % (e.code,e.reason,str(e.read()) if sys.version_info < (3, 0) else e.read().decode() )
    # html = e.read()
    # print("Content-type: text/html; charset=utf-8\n")
    # raise e
    # quit()
    error=True
    # status="%s %s" % (e.code,e.reason)


htmls=html

# print(mimetype)
# print(info)
# raise Exception(str(response)+ "-----status:"+str(response.status)+'-----code:'+str(response.code))
if amimetypes[0]!="image" and amimetypes[0]!="img" and not (len(amimetypes)>1 and amimetypes[1] == 'octet-stream') and not error and (response.status is None or not (response.status>=300 and response.status<400)) and (response.code is None or not (response.code>=300 and response.code<400)) :
    try:
        htmls = str(html) if sys.version_info < (3, 0) else str(html,"utf-8")
        # htmls = htmls.replace('href="/','href="/'+scriptfolder+repl_suff).replace("href='/","href='/"+scriptfolder+"/?")
        # htmls = htmls.replace('src="/','src="/'+scriptfolder+repl_suff).replace("src='/","src='/"+scriptfolder+"/?")
        htmls=re.sub(r'\bhref(\s*[=:]\s*[\'"]\s*/?)(?!#)',r'href\g<1>/',htmls, flags=re.IGNORECASE)
        htmls=re.sub(r'\bsrc(\s*[=:]\s*[\'"]\s*/?)',r'src\g<1>/',htmls, flags=re.IGNORECASE)
        htmls=re.sub(r'\burl(\s*[=:]\s*[\'"]\s*/?)',r'url\g<1>/',htmls, flags=re.IGNORECASE)
        htmls=re.sub(r'\bredirect-url(\s*[=:]\s*[\'"]\s*/?)',r'redirect-url\g<1>/',htmls, flags=re.IGNORECASE)
        htmls=re.sub(r'_url(\s*[=:]\s*[\'"]\s*/?)',r'_url\g<1>/',htmls, flags=re.IGNORECASE)
        htmls=re.sub(r'\badvanceUrl(\s*[=:]\s*[\'"]\s*/?)',r'advanceUrl\g<1>/',htmls)
        htmls=re.sub(r'\burl(\s*\(\s*[\'"]?\s*/?)(\.\.|/)',r'url\g<1>\g<2>/',htmls, flags=re.IGNORECASE)
        
        htmls=re.sub(r'\bhref(\s*[=:]\s*[\'"]\s*)//',r'href\g<1>/',htmls)
        htmls=re.sub(r'\bsrc(\s*[=:]\s*[\'"]\s*)//',r'src\g<1>/',htmls)
        htmls=re.sub(r'\burl(\s*[=:]\s*[\'"]\s*)//',r'url\g<1>/',htmls)
        htmls=re.sub(r'\bredirect-url(\s*[=:]\s*[\'"]\s*)//',r'redirect-url\g<1>/',htmls)
        htmls=re.sub(r'_url(\s*[=:]\s*[\'"]\s*)//',r'_url\g<1>/',htmls)
        htmls=re.sub(r'\badvanceUrl(\s*[=:]\s*[\'"]\s*)//',r'advanceUrl\g<1>/',htmls)
        htmls=re.sub(r'\burl(\s*\(\s*[\'"]?\s*)(\.\.|/)//',r'url\g<1>\g<2>/',htmls)
        
        htmls=re.sub(r'\bhref(\s*[=:]\s*[\'"]\s*)/(http://|https://)',r'href\g<1>\g<2>',htmls)
        htmls=re.sub(r'\bsrc(\s*[=:]\s*[\'"]\s*)/(http://|https://)',r'src\g<1>\g<2>',htmls)
        htmls=re.sub(r'\burl(\s*[=:]\s*[\'"]\s*)/(http://|https://)',r'url\g<1>\g<2>',htmls)
        htmls=re.sub(r'\bredirect-url(\s*[=:]\s*[\'"]\s*)/(http://|https://)',r'redirect-url\g<1>\g<2>',htmls)
        htmls=re.sub(r'_url(\s*[=:]\s*[\'"]\s*)/(http://|https://)',r'_url\g<1>\g<2>',htmls)
        htmls=re.sub(r'\badvanceUrl(\s*[=:]\s*[\'"]\s*)/(http://|https://)',r'advanceUrl\g<1>\g<2>',htmls)
        htmls=re.sub(r'\burl(\s*\(\s*[\'"]?\s*)(\.\.|/)/(http://|https://)',r'url\g<1>\g<2>\g<3>',htmls)
        
        htmls=re.sub(r'\bhref(\s*[=:]\s*[\'"]?\s*)/',r'href\g<1>/'+scriptfolder+repl_suff,htmls)
        htmls=re.sub(r'\bsrc(\s*[=:]\s*[\'"]?\s*)/',r'src\g<1>/'+scriptfolder+repl_suff,htmls)
        htmls=re.sub(r'\.attr(\(\s*[\'"]src[\'"]\s*,\s*[\'"]\s*)\/([^\'"]+)([\'"]\s*\))',r'.attr\g<1>/'+scriptfolder+repl_suff+r'\g<2>\g<3>',htmls)
        htmls=re.sub(r'\burl(\s*[:]\s*[\'"]?\s*)/',r'url\g<1>/'+scriptfolder+repl_suff,htmls)
        htmls=re.sub(r'\bredirect-url(\s*[:=]\s*[\'"]?\s*)/',r'redirect-url\g<1>'+protocol+'://'+httphost+'/'+scriptfolder+repl_suff,htmls)
        htmls=re.sub(r'\badvanceUrl(\s*[:=]\s*[\'"]?\s*)/',r'advanceUrl\g<1>/'+scriptfolder+repl_suff,htmls)
        htmls=re.sub(r'\_url(\s*[:]\s*[\'"]?\s*)/',r'_url\g<1>/'+scriptfolder+repl_suff,htmls)
        htmls=re.sub(r'\burl(\s*\(\s*[\'"]?\s*)(\.\.|/)/',r'url\g<1>/'+scriptfolder+repl_suff+r'\g<2>',htmls)
        if "reconnecting-websocket-iife.min.js" in finalpath: htmls = htmls.replace(',connectionTimeout:4e3,',',connectionTimeout:8e3,')
        if not method_pathinfo: 
            scriptfolderaction=scriptfolder+"/redirect_action.php"
            htmls=re.sub(r'\baction(\s*[=:]\s*[\'"]?\s*)/',r'action\g<1>/'+scriptfolderaction+'/',htmls)
        else:
            htmls=re.sub(r'\baction(\s*[=:]\s*[\'"]?\s*)/',r'action\g<1>/'+scriptfolder+repl_suff,htmls)
        htmls = htmls.replace('http://localhost:%s'%port+'/', protocol+'://'+httphost+'/'+scriptfolder+repl_suff)
        htmls = htmls.replace('https://localhost:%s'%port+'/', protocol+'://'+httphost+'/'+scriptfolder+repl_suff)
        htmls=re.sub(r'\bhref(\s*[=:]\s*[\'"]?\s*)/'+scriptfolder+repl_suff.replace('?','\\?')+'/',r'href\g<1>//',htmls)
        htmls=re.sub(r'\bsrc(\s*[=:]\s*[\'"]?\s*)/'+scriptfolder+repl_suff.replace('?','\\?')+'/',r'src\g<1>//',htmls)
        htmls=re.sub(r'\burl(\s*[=:]\s*[\'"]?\s*)/'+scriptfolder+repl_suff.replace('?','\\?')+'/',r'url\g<1>//',htmls)
        htmls=re.sub(r'\badvanceUrl(\s*[=:]\s*[\'"]?\s*)/'+scriptfolder+repl_suff.replace('?','\\?')+'/',r'advanceUrl\g<1>//',htmls)
        if len(lqs)>2: htmls = htmls.replace('/'+scriptfolder+repl_suff+'../..','/'+scriptfolder+repl_suff+lqs[-3]+"/")
        if len(lqs)>1: htmls = htmls.replace('/'+scriptfolder+repl_suff+'..','/'+scriptfolder+repl_suff+lqs[-2]+"/")
        # htmls = htmls.replace('action="/','action="/'+scriptfolderaction+'/').replace("action='/","action='/"+scriptfolderaction+"/")
        htmls=re.sub(r'\bhref(\s*[=:]\s*[\'"]?\s*)/'+scriptfolder+repl_suff.replace('?','\\?')+'javascript[:]',r'href\g<1>javascript:',htmls)
        if not method_pathinfo: htmls = htmls.replace('sourceMappingURL=','sourceMappingURL='+smurl_pref+'/')
        htmls=re.sub(r'\bnew\s+WebSocket\b', 'new FalseWebSocket', htmls)
        htmls=re.sub(r'\breturn\s+WebSocket\b', 'return FalseWebSocket', htmls)
        htmls=re.sub(r'\.WebSocket\b', '.FalseWebSocket', htmls)
        htmls=re.sub(r'(?i)(\n</head>)', '<script src="'+protocol+'://'+httphost+'/'+scriptfolder+r'/false-websocket.js" type="text/javascript"></script>\n\n\g<1>', htmls)
        cntt=mimetype+"; charset=utf-8"
    except Exception as excp:
        pass
        # htmls=str(excp)

envvars=""
# if error: print("Status: %s\n" % status)

if(sys.platform=="win32"):
    import msvcrt
    msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

httpinfo=""
if not error:  
    for infline in ainfo:
        if 'content-length:' not in infline.lower():
            if(httpinfo!=""): httpinfo+='\n'
            if mimetype=="application/json" and 'content-type:' in infline.lower() : infline=infline.replace('application/octet-stream',mimetype)      
            if not re.search(r'(?i)\bLocation\s*:\s*/',infline.strip()) is None:
                newinfline = infline.replace('http://localhost:%s'%port+'/', protocol+'://'+httphost+'/'+scriptfolder+repl_suff)
                newinfline=re.sub(r'(?i)\bLocation\s*:\s*/','Location: '+protocol+'://'+httphost+'/'+scriptfolder+repl_suff,newinfline)
                httpinfo+=newinfline
            elif not re.search(r'(?i)\bLocation\s*:\s*http[s]?://localhost',infline.strip()) is None:
                newinfline = infline
                newinfline=re.sub(r'(?i)\bLocation\s*:\s*http[s]?://localhost:'+str(port)+'/','Location: '+protocol+'://'+httphost+'/'+scriptfolder+repl_suff,newinfline)
                httpinfo+=newinfline
            else: httpinfo+=infline
        elif mimetype!="application/json": 
            if(httpinfo!=""): httpinfo+='\n'
            httpinfo+="Content-Length: %s"%(len(htmls))
            
    # print(httpinfo)
    # print(info)
    # if logdata: logging.info('Writing ModifiedRespHeaders to %s: {\n---\n%s\n---\n}\n'%(sys.stdout.fileno(),httpinfo))
    fp = os.fdopen(sys.stdout.fileno(), 'wb')
    # httpinfo=info
    fp.write(httpinfo+b"\n" if sys.version_info < (3, 0) or type(httpinfo) is bytes else httpinfo.encode())
    # fp.write(httpinfo if sys.version_info < (3, 0) or type(httpinfo) is bytes else httpinfo.encode())
    # fp.write(httpinfo if sys.version_info < (3, 0) or type(httpinfo) is bytes else httpinfo.encode())
    # htmls=html
    if ((response.status is None or not (response.status>=300 and response.status<400)) and (response.code is None or not (response.code>=300 and response.code<400))): fp.write(htmls if sys.version_info < (3, 0) or type(htmls) is bytes else htmls.encode())
    # fp.write(httpinfo.replace('\n','\n<br>').encode())
    fp.flush()
else: 
    fp = os.fdopen(sys.stdout.fileno(), 'wb')
    fp.write("Content-type: text/html; charset=utf-8\n\n".encode())
    fp.write(htmls if sys.version_info < (3, 0) or type(htmls) is bytes else htmls.encode())
    fp.flush()
if logdata:
    for param in os.environ.keys(): envvars="%s; %s:%s" % (envvars,param,os.environ[param])
    logging.info('Script_uri: %s. Base_addr: %s. Path QS: %s. Final path: %s. Data: %s. Mime: %s. Status: %s. Protocol: %s. RespHeaders: {\n---\n%s\n---\n} ModifiedRespHeaders: {\n---\n%s\n---\n}\n ClientHeaders: %s \nOpenerHeaders: %s \n::::::envvars::::::::\n%s\n:::::::::::\n repl_suff = %s\n:::::::::::\n'%(scripturi,base_addr,qs,finalpath,data,mime,status,protocol,info,httpinfo,clientheaders,opener.addheaders,envvars,repl_suff))
