<?php
if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket" || empty($_POST['ui']) || !isset($_POST['messagecount'])) exit("Your are not authorized.");
require_once("fwshelper.php");
// $id = $argv[1];
// $mcount=$argv[2];
$id = $_POST['ui'];
$mcount=$_POST['messagecount'];
$file="mess_".$id;
$folder=empty($config["wsdata_path"])?"wsdata":$config["wsdata_path"];
$folder.="/";
$confirmfilepath=$folder."confirm_".$id;
$resp=false;
if(isset($GLOBALS["max_exec_time"]) && ini_get("max_execution_time")<$GLOBALS["max_exec_time"]) ini_set('max_execution_time', $GLOBALS["max_exec_time"]);
$endtimerint=ini_get('max_execution_time')-2;
if($endtimerint<=0) $endtimerint=110;
$starttime=microtime(true);
$nloop=0; $nmaxloop=1;
$response = null;
while(($nmaxloop==0 || $nloop<$nmaxloop) && ($resp === false || $resp === '')) {
	$resp=getNewLine($folder.$file,$mcount);
	$nloop++;
	if($resp === false || $resp === '') {
		if($nmaxloop==0 || $nloop<$nmaxloop) usleep(100000);
		$ctime=microtime(true);
		if(!empty($GLOBALS["log_loops"])) $written=file_put_contents("mllog.log","- $nloop ($id): ".date("Y-m-d H:i:s").PHP_EOL, FILE_APPEND | LOCK_EX);
		$conf=getNewLine($confirmfilepath);
		if(!empty($conf)) {
			if($conf!="finished") $GLOBALS["confirmtime"]=strtotime($conf);
			else {
				$response = array('conn_closed'=>true, 'reason'=>'finished');
				break;
			}
		}
		if($ctime-$GLOBALS["confirmtime"]>$GLOBALS["confirm_delay"]) {
			$response = array('conn_closed'=>true, 'reason'=>'confirmtimeout');
			break;
		}
		if($ctime-$starttime>$endtimerint) {
			$response = array('conn_closed'=>true, 'reason'=>'timeout');
			break;
		}
		$cconfigcontents=file_get_contents("config.json");
		if( $cconfigcontents !== false) $cconfig=json_decode($configcontents,true);
		if(!empty($cconfig["emergency_stop"])) {
			$response = array('conn_closed'=>true, 'reason'=>'Sorry, the server is temporary switched off for maintenance/ Désolé, le serveur est temporairement en maintenance');
			break;
		}
	}
}
$respinit=$resp;
if($resp=="!close!") {
	$response = array('conn_closed'=>true, 'reason'=>'ext_close');
	$resp = json_encode($response);
}
// $filestodelete=[$folder.$file,$confirmfilepath];
$filestodelete=[$folder.$file];
$deletewords=[]; $deletefilenames=[];
foreach($filestodelete as $k=>$v) {
	$deletewords[$k]=(!file_exists($v))?"didn't exist":"deleted";
	$deletefilenames[$k]=($v==$folder.$file)?"message file":"confirm file";
}
if($resp=='closed:normal' || $resp=='error:confirmtimeout') {				
	$filesdeleted=forcedeletefile($filestodelete);
}
if(!empty($response['conn_closed'])) {
	$filesdeleted=forcedeletefile($filestodelete);
}
	
if($resp === null) {
	$response = array('conn_closed'=>true, 'reason'=>'error:impossible to lock file '.$folder.$file);
	$resp = json_encode($response);
}
if($resp === false || $resp === '') {
	$resp = json_encode($response);
	if($response === null) $resp = 'wait';
}
if(!empty($GLOBALS["log_loops"])) {
	$delfileinfo="";
	foreach($filestodelete as $k=>$v) {
		$deletedthisfile=!file_exists($filestodelete[$k]);
		$delfileinfo.=', '.$deletefilenames[$k].' `'.$filestodelete[$k].'` '.($deletedthisfile?$deletewords[$k]:'NOT DELETED');
	}
	$written=file_put_contents("mllog.log","-  ($id): ".date("Y-m-d H:i:s")." closing after receiving `$respinit`, resp=`$resp`".$delfileinfo.PHP_EOL, FILE_APPEND | LOCK_EX);
}
// if(empty($response['reason']) || $response['reason']!="finished")  {}
echo $resp;
flush();
?>