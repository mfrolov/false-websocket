# False Websocket - App Tunnel

The project is intended to enable running websocket-based applications on websites which use apache hosting providers with ssh (or cPanel/terminal so that theses applications can be installed and run locally) but without websockets (apache mod_proxy_wstunnel and exposing of the application's specific port to the web not allowed)

## Installation
Copy/clone all files into a subfolder of the www folder of your site.

## Usage
- launch your application using ssh or cPanel/terminal so that it works for the localhost on a specific port
- change the 8000 port in the config.json file to the application's specific port
- cd to the supserver subfolder using ssh or cPanel/terminal and start ``` php supserver.php ``` or make the start_supserver file executable with ``` chmod 755 start_supserver ``` and execute  ``` ./start_supserver ```
- cd to the subfolder using ssh or cPanel/terminal and make index.cgi file executable (``` chmod 755 index.cgi ```) 
- browse to the subfolder/index.cgi file of your site (e.g. https://your-site.com/subfolder/index.cgi) from any external computer. It is also possible to omit index.cgi (e.g.  https://your-site.com/subfolder/). However, some complicated structures (such as using static .html files in iframes) are more chances to work when index.cgi is present.

#### Alternative usage (not recommended)
The application may work without launhing ``` php supserver.php ``` : change method: 'supserver' to method: 'comet' in the false-websocket.js file, line 97. In that case, each websocket request will launch two blocking processes on the server (changing "emergency_stop":0 to "emergency_stop":1 in the config.json file will stop the blocking processes from all not closed pages).

## Project status
The project is primary intended to work and has been tested with the otree application (https://www.otree.org/) but may work with other applications as well.


## Credits
- Using Ratchet - WebSockets for PHP (http://socketo.me/, https://github.com/ratchetphp/Pawl)
- ReconnectingWebSocket (https://github.com/joewalnes/reconnecting-websocket/blob/master/reconnecting-websocket.js) was used to develop false-websocket.js

## License
MIT License (https://mit-license.org/)

