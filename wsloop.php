<?php
if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket" || empty($_POST['ui']) || !isset($_POST['path']) || !isset($_POST['sendcount'])) exit("Your are not authorized.");
$id = $_POST['ui'];
$sendcount=$_POST['sendcount'];
$path=$_POST['path'];
// $id = $argv[1];
// $sendcount=$argv[2];
// $path=$argv[3];
$debug=false;

require_once("fwshelper.php");

$filesend="send_".$id;
$filemess="mess_".$id;
$folder=empty($config["wsdata_path"])?"wsdata":$config["wsdata_path"];
$folder.="/";
$headers = apache_request_headers();

if($debug) {
	if(!function_exists("apache_request_headers")) exit("Forbidden");
	
	$nows="";
	if(!empty($_GET["nows"])) $nows="(not webs socket)";
	$res="";
	foreach ($headers as $header => $value) {
		$res.= "$header: $value \n";
	}
	$res = date("Y-m-d H:i:s")." ".$nows.":\n:".$res."\n\n----\n\n";
	file_put_contents("headers.log",$res,FILE_APPEND);
}



if(file_exists(__DIR__ . '/vendor/autoload.php')) require __DIR__ . '/vendor/autoload.php';
else require __DIR__ . '/../vendor/autoload.php';

// $server="wss://echo.websocket.org:443";
$port=empty($config["port"])?8000:$config["port"];
// if(count($argv)>2) $port=$argv[2];
$server="ws://localhost:".$port."/".$path;
// $message='Hello World!'; if(count($argv)>1) $message=$argv[1];

function checkMessageToSend($conn,$csfile,&$sendcount) {
	static $n=0;
	$n++;
	$mess=getNewLine($csfile,$sendcount);
	if(!empty($GLOBALS["log_loops"])) $written=file_put_contents("wslog.log", "- ".($n)." (".$GLOBALS["id"].") (path ".$GLOBALS["path"]."): ".date("Y-m-d H:i:s").' : sendcount='.$sendcount.', mess:`'.$mess.'`'.PHP_EOL, FILE_APPEND | LOCK_EX);
	if($mess!==false && $mess!=='' ) {
		if($mess ==  "!close!") {
			if($GLOBALS["debug"]) echo "Closing connection...\n";
			$conn->close();
		}
		else {
			if($GLOBALS["debug"]) echo "Sending: {$mess}\n\n";
			$conn->send($mess);
		}
		$sendcount++;
	}
	else {
		$conffile=$GLOBALS["folder"]."confirm_".$GLOBALS["id"];
		$conf=getNewLine($conffile);
		if(!empty($conf)) {
			if($conf!="finished") $GLOBALS["confirmtime"]=strtotime($conf);
			else {
				if($GLOBALS["debug"]) echo "Closing connection...\n";
				$conn->close(1000,"closed:finished");
				return;
			}
		}
		$ctime=microtime(true);
		if($ctime-$GLOBALS["confirmtime"]>$GLOBALS["confirm_delay"]) {
			$conn->close(1003,"error:confirmtimeout");
			return;
		}
		$cconfigcontents=file_get_contents("config.json");
		if( $cconfigcontents !== false) $cconfig=json_decode($cconfigcontents,true);
		if(!empty($cconfig["emergency_stop"])) {
			$conn->close(1001,"closed:finished");
			die("error:Sorry, the server is temporary switched off for maintenance/ Désolé, le serveur est temporairement en maintenance");
		}
	}
}

$loop = \React\EventLoop\Factory::create();
$reactConnector = new \React\Socket\Connector($loop);
$connector = new \Ratchet\Client\Connector($loop, $reactConnector);

$connheaders=[];
if(array_key_exists("Cookie",$headers)) $connheaders["Cookie"]=$headers["Cookie"];
$connector($server,[],$connheaders)
->then(function(Ratchet\Client\WebSocket $conn) use($loop, $folder, $filemess, $filesend,  &$sendcount) {
	$conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($loop, $conn, $folder, $filemess, &$sendcount) {
		if($GLOBALS["debug"]) echo "Received: {$msg}\n";
		$scriptfolder='otree';
		if(!empty($_SERVER["REQUEST_URI"])) {
			$areuri=explode('/',$_SERVER["REQUEST_URI"]);
			$scriptname=array_pop($areuri);
			if($areuri[0]=="") array_shift($areuri);
			$qsarr=explode('?',implode('/',$areuri));
			$scriptfolder=rtrim($qsarr[0],"/");
		}
		$repl_suff='/?'; if(!empty($_POST["replsuff"])) $repl_suff=$_POST["replsuff"];
		$msg=preg_replace('#\burl(\'|")?\s*:\s*(\'|")\s*/#','url$1: $2/'.$scriptfolder.$repl_suff,$msg);
		$msg=preg_replace('#_url(\'|")?\s*:\s*(\'|")\s*/#','_url$1: $2/'.$scriptfolder.$repl_suff,$msg);
	   // $conn->close();
		$cmfile=$folder.$filemess;
		$written=file_put_contents($cmfile, $msg.chr(30), FILE_APPEND | LOCK_EX);
		if(!$written) {
			$conn->close(1002,'error:writing_file:'.$cmfile);
			$loop->stop();
		}
	});

	$csfile=$folder.$filesend;
	$conn->on('close', function($code = null, $reason = null) use ($loop, $csfile) {
		// $response = array('conn_closed'=>true, 'code' =>$code, 'reason' =>$reason);
		// $resp = json_encode($response);
		$resp='closed:normal';
		if(!empty($reason)) $resp=$reason;
		if($reason !== 'error:timeout') {
			if(isset($GLOBALS["pertimer"])) $loop->cancelTimer($GLOBALS["pertimer"]);
			if(isset($GLOBALS["endtimer"])) $loop->cancelTimer($GLOBALS["endtimer"]);
		}
		// $filestodelete=[$csfile,$GLOBALS["folder"]."confirm_".$GLOBALS["id"]];
		$filestodelete=[$csfile];
		$deletewords=[]; $deletefilenames=[];
		foreach($filestodelete as $k=>$v) {
			$deletewords[$k]=(!file_exists($v))?"didn't exist":"deleted";
			$deletefilenames[$k]=($v==$csfile)?"sendfile":"confirmfile";
		}
		if($resp=='closed:normal' || $resp=='error:confirmtimeout') {				
			$filesdeleted=forcedeletefile($filestodelete);
		}
		$delfileinfo="";
		foreach($filestodelete as $k=>$v) {
			$deletedthisfile=!file_exists($filestodelete[$k]);
			$delfileinfo.=', '.$deletefilenames[$k].' `'.$filestodelete[$k].'` '.($deletedthisfile?$deletewords[$k]:'NOT DELETED');
		}
		if(!empty($GLOBALS["log_loops"])) $written=file_put_contents("wslog.log", "- connection closed (".$GLOBALS["id"]."): ".date("Y-m-d H:i:s").' : reason='.$reason.', resp:`'.$resp.'`'.$delfileinfo.PHP_EOL, FILE_APPEND | LOCK_EX);
		echo $resp;
		$loop->stop();
	});
	

	checkMessageToSend($conn,$csfile,$sendcount);

	$GLOBALS["pertimer"] = $loop->addPeriodicTimer(0.24, function () use ($conn,$csfile,&$sendcount) {
		checkMessageToSend($conn,$csfile,$sendcount);
	});
	
	// ini_set('max_execution_time', '7200');
	if(isset($GLOBALS["max_exec_time"]) && ini_get("max_execution_time")<$GLOBALS["max_exec_time"]) ini_set('max_execution_time', $GLOBALS["max_exec_time"]);
	$endtimerint=ini_get('max_execution_time')-2;
	if($endtimerint<=0) $endtimerint=110;
	// echo "max_execution_time:".ini_get('max_execution_time').", endtimerint:".$endtimerint."\n";

	$GLOBALS["endtimer"]=$loop->addTimer($endtimerint, function () use ($loop, $conn) {
		$loop->cancelTimer($GLOBALS["pertimer"]);
		$conn->close(1001, 'error:timeout');
		
	});
}, function(\Exception $e) use ($loop) {
	echo "error:connection: {$e->getMessage()}\n";
	$loop->stop();
});

$loop->run();
	
?>