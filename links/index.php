<?php

function getRedirect($url){
	// Initialize the cURL
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
	$aheaders=apache_request_headers();
	$cheader=array("Cookie: ".$aheaders["Cookie"]);
	// var_dump($cheader);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $cheader);
	
	curl_exec($ch); 

	$RedirectedUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); 

	curl_close($ch);

	return $RedirectedUrl; 
}

$aprotocol=explode("/",$_SERVER['SERVER_PROTOCOL']);
$http=empty($_SERVER['HTTPS'])?strtolower($aprotocol[0]):"https";
$ascriptname=explode("/",$_SERVER['SCRIPT_NAME']);
$filename=array_pop($ascriptname);
$links=array_pop($ascriptname);
$newhttpfolder=$http."://".$_SERVER['SERVER_NAME'].implode("/",$ascriptname);

$returnedurl=getRedirect($newhttpfolder);
$ok=preg_match("/\bdemo$/",$returnedurl) && !preg_match("/\blogin$/",$returnedurl);

// exit;
if(!$ok) {
	header('Location: ../');
	exit;
}
	
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$acceptLang = ['fr', 'en']; 
$lang = in_array($lang, $acceptLang) ? $lang : 'en';

$fileforlinks="links.txt";
$message="";
$links=[];
// if(!empty($_POST)) var_dump($_POST);
if(!empty($_POST["nlinks"]) && is_numeric($_POST["nlinks"])) {
	$towrite="";
	$nlinks=0; $alerts="";
	for($i=1; $i<=$_POST["nlinks"]; $i++) if(!empty($_POST["lnk".$i])) {
		$clink=$_POST["lnk".$i];
		$aclink=explode('://',$clink);
		// var_dump($aclink);
		if(substr(strtolower($aclink[0]),0,4)=="http" && !empty($aclink[1]))
		{
			$towrite.=$clink.PHP_EOL;
			$nlinks++;
		}
		else $alerts.=(($lang=="fr")?"Le lien n° $i est incorrect. ":"The link n# $i is incorrect. ");
	}
	if(!empty($towrite)) {
		file_put_contents($fileforlinks,$towrite);
	}
	elseif($nlinks==0 && file_exists($fileforlinks)) {
		file_put_contents($fileforlinks,"");
	}
	if($nlinks>0) {
		$message.="<div class='alert alert-success text-center'>".$nlinks.(($lang=="fr")?" lien(s) enregistré(s)":" link(s) registered")."</div>";
	}
	else $message.="<div class='alert alert-danger text-center'>".$nlinks.(($lang=="fr")?" lien(s) enregistré(s)":" link(s) registered")."</div>";
	if(!empty($alerts)) $message.="<div class='alert alert-warning text-center'>".$alerts."</div>";
}
if(file_exists($fileforlinks)) {
	$lconts=file_get_contents($fileforlinks);
	if(!empty($lconts)) $links=explode(PHP_EOL,$lconts);
}
// var_dump($links);

$protocol=$_SERVER['SERVER_PROTOCOL'];
$aprotocol=explode("/",$_SERVER['SERVER_PROTOCOL']);
$http=empty($_SERVER['HTTPS'])?strtolower($aprotocol[0]):"https";
$ascriptname=explode("/",$_SERVER['SCRIPT_NAME']);
$filename=array_pop($ascriptname);
$newhttpfolder=$http."://".$_SERVER['SERVER_NAME'].implode("/",$ascriptname);


$btn=($lang=="fr")?"Valider":"Validate";
$btnadd=($lang=="fr")?"Ajouter un autre lien":"Add another link";
$btcopy=($lang=="fr")?"Copier dans le press-papier":"Copy to clipboard";
$copied=($lang=="fr")?"La représentation du lien n° @ copié!":"The representation of the link n# @ copied!";
$dne= "Enter the links";
if($lang=="fr") $dne= "Entrer le ou les liens";

?>
<html lang="<?php echo  $lang; ?>">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://clipboardjs.com/bower_components/primer-css/css/primer.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>
  <script src="https://clipboardjs.com/bower_components/highlightjs/highlight.pack.min.js"></script>
  <script src="https://clipboardjs.com/assets/scripts/tooltips.js"></script>


</head>
<body>
<h4><?php echo $dne; ?></h4>
<?php echo $message; ?>
<div class="container text-center">
  <form method=POST>
	<input type="hidden" name=nlinks value=0 />
    <div class="form-group" id=maindiv>
	  
    </div>
    <div class="container text-right">
		<input type="button" class="btn btn-info" onclick="appendlink()" value="<?php echo $btnadd; ?>">
    </div>
    <div class="container text-center">
		<input type="submit" class="btn btn-danger" value="<?php echo $btn; ?>">
    </div>
  </form>
</div>
<script>
var cid=0;
var linktext="<?php echo ($lang=='fr')?'Lien n° ':'Link n# '; ?>"
var phrase="<?php echo ($lang=='fr')?'Utiliser en tant que ':'Use as '; ?>"
function appendlink(prevlink)
{
	if(prevlink==undefined) prevlink="";
	cid++;
	var link="<?php echo $newhttpfolder;?>"+"/redirect.php?participant_label={{%PROLIFIC_PID%}}";
	if(cid>1) link+="&linkid="+cid;
	var newrow="<div><hr style='width:50%; margin-left: 25%;' />";
	newrow+=' <label for="lnk'+cid+'">'+linktext+cid+'</label><input type="text" value="'+prevlink+'" class="form-control" name=lnk'+cid+' id="lnk'+cid+'">';
	newrow+=" "+phrase+" <strong>"+link+"</strong>";
	newrow+=" "+'<input type=button class="btn cpbtn" value="<?php echo $btcopy; ?>" data-clipboard-action="copy" title="'+cid+'" data-clipboard-text="'+link+'"/>';
	newrow+="</div>";
	$("#maindiv").append(newrow);
	$('input[name="nlinks"]').val(cid);
	// console.log($('input[name="nlinks"]').val());
	
}
$(document).ready(function(){
	<?php
		if(empty($links)) echo "appendlink();";
		else foreach($links as $clink) if(!empty($clink)) echo "appendlink('$clink');";
	?>
	
	var clipboard = new ClipboardJS('.cpbtn');

	clipboard.on('success', function(e) {
		console.info('Action:', e.action);
		console.info('Text:', e.text);
		console.info('Trigger:', e.trigger);
		alert("<?php echo $copied;?>".replace(/@/,e.trigger.title));
		// showTooltip(e.trigger,'Copied!');

		//e.clearSelection();
	});

	clipboard.on('error', function(e) {
		console.error('Action:', e.action);
		console.error('Trigger:', e.trigger);
	});
	
});
</script>
  
</body>
</html>