<?php
require_once("fwshelper.php");
if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket" || !isset($_POST['path']) ) exit("Your are not authorized.");
// $id = (!empty($_SERVER["UNIQUE_ID"]))?$_SERVER["UNIQUE_ID"]:uniqid("",true);
$id = uniqid("",true);
$id = str_replace(array('\\','/',':','*','?','"','<','>','|',' ','.',"'"),'',$id);
$scriptfolder='otree';
if(!empty($_SERVER["REQUEST_URI"])) {
	$areuri=explode('/',$_SERVER["REQUEST_URI"]);
	$scriptname=array_pop($areuri);
	if($areuri[0]=="") array_shift($areuri);
	$qsarr=explode('?',implode('/',$areuri));
	$scriptfolder=rtrim($qsarr[0],"/");
}
$headers = apache_request_headers();
$headers["scriptfolder"]=$scriptfolder;
$headers["path"]=$_POST['path'];
if(!empty($_POST["replsuff"])) $headers["replsuff"]=$_POST['replsuff'];
$headers["id"]=$id;
$headers["creation_time"]=microtime(true);
if(!empty($_POST['ignore'])) $headers["ignore"]=$_POST['ignore'];
$folder=empty($config["wsdata_path"])?"wsdata":$config["wsdata_path"];
$folder.="/";
$written=@file_put_contents($folder."handler_".$id,json_encode($headers),LOCK_EX);
if($written === false) echo "error:probem writing ".$folder."handler_".$id." file";
else echo $id;
?>