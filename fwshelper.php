<?php
$max_exec_time=600;
$confirm_delay=25;
$log_loops=false;
$confirmstarttime=microtime(true);
$confirmtime=$confirmstarttime;

$configcontents=file_get_contents(__DIR__ ."/config.json");
if( $configcontents !== false) $config=json_decode($configcontents,true);
// if(!empty($config["emergency_stop"]) && !defined("CLEAN")) exit("error:Sorry, the server is temporary switched off for maintenance/ Désolé, le serveur est temporairement en maintenance");
if(!empty($config["emergency_stop"]) && !defined("CLEAN")) exit("error:stopped");

function getNewLine($path, $countfrom = 0, $waitIfLocked = true) {
    if(!file_exists($path)) {
        // throw new Exception('File "'.$path.'" does not exists');
        return false;
    }
    else {
		$filesize=filesize($path);
        $fo = @fopen($path, 'r');
		if($fo === false) return false;
        $locked = flock($fo, LOCK_SH, $waitIfLocked);
       
        if(!$locked) {
            return null;
        }
        else {
			
			$cts=false;
			$nline=0;
			while($line=stream_get_line($fo,max(1024,$filesize),chr(30))) {
				$nline++;
				if($nline>$countfrom) {
					// echo "lu:".$line."\n";
					$cts=$line;
					break;
				}
			}
           
            flock($fo, LOCK_UN);
            fclose($fo);
           
            return $cts;
        }
    }
}

function forcedeletefile($path,$nitermax=100,$delayms=20)
{
	if(!is_array($path)) $path=array($path);
	$oneexists=function($carry,$item) {return ($carry || file_exists($item));};
	$deleted=[];
	foreach($path as $key=>$item) {$deleted[$key]=false;}
	$n=0;
	while(array_reduce($path,$oneexists,false)) {
		foreach($path as $key=>$item) {
			if(file_exists($item)) $deleted[$key]=@unlink($item);
		}
		if($n>$nitermax) break;
		if(array_reduce($path,$oneexists,false)) usleep(1000*$delayms);
		$n++;
	}
	return $deleted;
}
?>