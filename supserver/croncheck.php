<?php
if(function_exists("apache_request_headers") && !empty(apache_request_headers())) exit("Forbidden");

$debug=false;
chdir(__DIR__);

$supservfolder=str_replace("\\","/",getcwd());
$logfile=$supservfolder."/"."cronlog.log";

$path=explode("/",$supservfolder);
$rpath=array_reverse($path);
$foldername=$rpath[1];
$otree_path="/s2ch/otree/".$foldername;
$configcontents=file_get_contents($supservfolder ."/../config.json");
if( $configcontents !== false) $config=json_decode($configcontents,true);
if(!empty($config["otree_path"])) $otree_path=$config["otree_path"];

$appname="otree"; // the application's name

if(file_exists("pid.txt"))
{
	$pid = trim(file_get_contents( "pid.txt" ));
	if($pid!="stopped" && is_numeric($pid)) {
		if( !file_exists( "/proc/$pid" )) {
			error_log(date("Y-m-d H:i:s")." : process $pid - supserver was not active, restarting\n",3,$logfile);
			exec("./start_supserver");
		}
		elseif($debug) echo "-process supserver $pid is running\n";
	}
	elseif($debug) echo "-process supserver was $pid";
}
elseif($debug) echo "-file pid.txt did not exist if folder $supservfolder\n";

chdir($otree_path); // the application's folder
if( file_exists("pid.txt"))
{
	$pid = trim(file_get_contents( "pid.txt" ));
	if($pid!="stopped" && is_numeric($pid)) {
		if(!file_exists( "/proc/$pid" )) {
			error_log(date("Y-m-d H:i:s")." : process $pid - $appname was not active, restarting\n",3,$logfile);
			exec("./start_exp"); // executable which starts the application
		}
		elseif($debug) echo "process $appname $pid is running\n";
	}
	elseif($debug) echo "-process $appname was $pid";
}
elseif($debug) echo "-file pid.txt did not exist if folder ".getcwd()."\n";

?>
