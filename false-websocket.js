// MIT License:
//
// Copyright (c) 2020 Maxim Frolov ,PSE/LEEP
// Based on ReconnectingWebSocket (Copyright (c) 2010-2012, Joe Walnes)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/**
 * This behaves like a WebSocket in every way, except if it fails to connect,
 * or it gets disconnected, it will repeatedly poll until it successfully connects
 * again.
 *
 * It is API compatible, so when you have:
 *   ws = new WebSocket('ws://....');
 * you can replace with:
 *   ws = new FalseWebSocket('ws://....');
 *
 * The event stream will typically look like:
 *  onconnecting
 *  onopen
 *  onmessage
 *  onmessage
 *  onclose // lost connection
 *  onconnecting
 *  onopen  // sometime later...
 *  onmessage
 *  onmessage
 *  etc...
 *
 * It is API compatible with the standard WebSocket API, apart from the following members:
 *
 * - `bufferedAmount`
 * - `extensions`
 * - `binaryType`
 *
 * Syntax
 * ======
 * var socket = new FalseWebSocket(url, protocols, options);
 *
 * Parameters
 * ==========
 * url - The url you are connecting to.
 * protocols - Optional string or array of protocols.
 * options - See below
 *
 * Options
 * =======
 * Options can either be passed upon instantiation or set after instantiation:
 *
 * var socket = new FalseWebSocket(url, null, { debug: true, reconnectInterval: 4000 });
 *
 * or
 *
 * var socket = new FalseWebSocket(url);
 * socket.debug = true;

 */
(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof module !== 'undefined' && module.exports){
        module.exports = factory();
    } else {
        global.FalseWebSocket = factory();
    }
})(this, function () {

    if (!('WebSocket' in window)) {
        return;
    }

    function FalseWebSocket(url, protocols, options) {

        // Default settings
        var settings = {

            /** Whether this instance should log debug messages. */
            debug: false,
			
			/** 'comet' for automatically launching two supplementary processes by page, 'supserver' for short ajax requests each messageFinderInterval ms after launching php supserver.php on the server via ssh. */
			method: 'supserver',
			
			/** paths not to treat */
			paths2ignore: ['no_op/'], //'no_op/'

            /** Whether or not the websocket should attempt to connect immediately upon instantiation. */
            automaticOpen: true,

            /** The number of milliseconds to delay before attempting to reconnect. */
            reconnectInterval: 1000,
            /** The maximum number of milliseconds to delay a reconnection attempt. */
            maxReconnectInterval: 30000,
            /** The rate of increase of the reconnect delay. Allows reconnect attempts to back off when problems persist. */
            reconnectDecay: 1.5,

            /** The maximum time in milliseconds to wait for a connection to succeed before closing and retrying. */
            timeoutInterval: 2000,
			confirmDelay:5000,
			messageFinderInterval: 500,

            /** The maximum number of reconnection attempts to make. Unlimited if null. */
            maxReconnectAttempts: 0,
            maxWsReconn: 5,
            maxMessReconn: 5,
            maxSendAttempts: 5,
            maxMessErrors: 500,

            /** The binary type, possible values 'blob' or 'arraybuffer', default 'blob'. */
            binaryType: 'blob'
        }
        if (!options) { options = {}; }

        // Overwrite and define settings with options if they exist.
        for (var key in settings) {
            if (typeof options[key] !== 'undefined') {
                this[key] = options[key];
            } else {
                this[key] = settings[key];
            }
        }

        // These should be treated as read-only properties

        /** The URL as resolved by the constructor. This is always an absolute URL. Read only. */
        this.url = url;
        this.baseurl = null;
        this.host = null;
        this.path = '';
		this.messageFinderHandlers={};
		this.messageFinderStatuses={};

        /** The number of attempted reconnects since starting, or the last successful connection. Read only. */
        this.reconnectAttempts = 0;
        this.wsLoopReconn = 0;
        this.wsMessReconn = 0;
        this.sendingAttempt = 0;
        this.messageFinderCount = 0;
        this.messageFoundCount = 0;

        /**
         * The current state of the connection.
         * Can be one of: WebSocket.CONNECTING, WebSocket.OPEN, WebSocket.CLOSING, WebSocket.CLOSED
         * Read only.
         */
        this.readyState = WebSocket.CONNECTING;
		
        /**
         * The Unique Identifier
         */

		this.UI = 0;
		this.messageCount = 0;
		this.sendCount = 0;
		this.starttime = 0;


        this.protocol = null;
		
		this.confirmInterval=null;
		this.findMessageInterval=null;
		this.msloopHandler=null;
		this.wsloopHandler=null;

        // Private state variables
		
		this.repl_suff='/?'
		this.urlprefix='';
		var aprurl=window.location.href.split('/');
		
		var tempurlpref='';
		for(var i=0; i<aprurl.length; i++) {
			if(aprurl[i].replace(/\.cgi$/i,'')!=aprurl[i]) {
				this.urlprefix=tempurlpref;
				this.repl_suff='/'+aprurl[i]+'/'
				break;
			}
			tempurlpref+=aprurl[i]+"/";
		}
		

        var self = this;
        var ws;
        var forcedClose = false;
        var timedOut = false;
        var eventTarget = document.createElement('div');

        // Wire up "on*" properties as event handlers

        eventTarget.addEventListener('open',       function(event) { self.onopen(event); });
        eventTarget.addEventListener('close',      function(event) { self.onclose(event); });
        eventTarget.addEventListener('connecting', function(event) { self.onconnecting(event); });
        eventTarget.addEventListener('message',    function(event) { self.onmessage(event); });
        eventTarget.addEventListener('error',      function(event) { self.onerror(event); });
        eventTarget.addEventListener('beforeunload',function(event) { if(self.debug || FalseWebSocket.debugAll) console.debug('beforeunload event'); self.clearPendingTasks(); self.confirmFalseWebSocket("finished"); delete event['returnValue'];});

        // Expose the API required by EventTarget

        this.addEventListener = eventTarget.addEventListener.bind(eventTarget);
        this.removeEventListener = eventTarget.removeEventListener.bind(eventTarget);
        this.dispatchEvent = eventTarget.dispatchEvent.bind(eventTarget);

        /**
         * This function generates an event that is compatible with standard
         * compliant browsers and IE9 - IE11
         *
         * This will prevent the error:
         * Object doesn't support this action
         *
         * http://stackoverflow.com/questions/19345392/why-arent-my-parameters-getting-passed-through-to-a-dispatched-event/19345563#19345563
         * @param s String The name that the event should use
         * @param args Object an optional object that the event will use
         */
        function generateEvent(s, args) {
        	var evt = document.createEvent("CustomEvent");
        	evt.initCustomEvent(s, false, false, args);
        	return evt;
        };
		function testJSON(text) { 
			if (typeof text !== "string") { 
				return false; 
			} 
			try { 
				JSON.parse(text); 
				return true; 
			} catch (error) { 
				return false; 
			} 
		}
		
		
		function generateError(textStatus, error, code)
		{
			if(self.UI==0) return;
			if (self.debug || FalseWebSocket.debugAll) {
				console.debug('FalseWebSocket', 'generating error event:'+code+'|'+error, "UI="+self.UI);
			}
			self.clearPendingTasks();
			self.confirmFalseWebSocket("finished");
			self.readyState = WebSocket.CLOSING;
			if(code == undefined) code=-1;
			if(error == undefined) error="";
			if(textStatus == undefined) textStatus="";
			var e = generateEvent('error');
			e.code = code;
			e.status = textStatus;
			eventTarget.dispatchEvent(e);
			
		}

        this.connect = function (reconnectAttempt) {
			ws = new Object();
            // ws = new WebSocket(self.url, protocols || []);
            ws.binaryType = this.binaryType;
			var aurl=self.url.split(':');
            self.protocol = aurl.shift();
			self.baseurl = aurl.join(':');
			var abaseurl=self.baseurl.substr(2).split("/");
			self.host=abaseurl.shift();
			self.path=abaseurl.join("/");

            if (reconnectAttempt) {
                if (this.maxReconnectAttempts && this.reconnectAttempts > this.maxReconnectAttempts) {
                    return;
                }
            } else {
               this.reconnectAttempts = 0;
            }
			if(this.readyState != WebSocket.OPEN) {
				this.readyState = WebSocket.CONNECTING;
				eventTarget.dispatchEvent(generateEvent('connecting'));
			}
 
            if (self.debug || FalseWebSocket.debugAll) {
                console.debug('FalseWebSocket', 'attempt-connect', self.url);
            }



			$.ajax({
				type: 'POST',
				url: (self.method=='comet')?self.urlprefix+'gethandler.php':self.urlprefix+'globalhandler.php',     
				data: {key: 'FalseWebSocket', path:self.path, replsuff:self.repl_suff, ignore:(self.paths2ignore.indexOf(self.path)<0)?'0':'1'},
				dataType: 'text',
				cache: false,
				timeout: 7000,
				success: function(data) {
					adata=data.split(':');
					if(data!="Your are not authorized." && adata[0]!="error") {
						if (self.debug || FalseWebSocket.debugAll) {
							console.debug('FalseWebSocket', 'connected. UI='+data, self.url);
							// let ws_scheme = window.location.protocol === "https:" ? "wss" : "ws";
							// let ws_path = `${ws_scheme}://${window.location.host}/${window.location.pathname}getheaders/`;
							// console.debug("ws_path:",ws_path);
							// let socket = new WebSocket(ws_path);
						}
						self.UI=data;
						// clearTimeout(timeout);
						self.starttime=(new Date()).getTime();
						self.open(reconnectAttempt);
					}
					else {
						var debdescr=data, errkey=adata[0], errdescr=(adata.length>1)?adata[1]:"unknown";
						if(adata[0]!="error") {
							errkey="BadKey";
							debgdescr="connecting error: bad key";
							errdescr="Bad key provided for gethandler.php";
						}
						if (self.debug || FalseWebSocket.debugAll) {
							console.debug('FalseWebSocket', debdescr, self.url);
						}
						// self.readyState = WebSocket.CLOSING;
						generateError(errkey, errdescr);
					}
				},
				error: function(jqXHR, textStatus, error) {
					// clearTimeout(timeout);
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'connecting error:'+textStatus+'|'+error, self.url);
					}
					// self.readyState = WebSocket.CLOSED;
					generateError(textStatus, error);
					//console.log("-connecting error:"+textStatus+"|"+error);
				}
			});
		}
		
        this.open = function (reconnectAttempt) {			
			self.reconnectAttempts = 0;
			reconnectAttempt = false;
			if (self.debug || FalseWebSocket.debugAll) {
				console.debug('FalseWebSocket', 'onopen ui:'+self.UI+', messagecount:'+self.messageCount+', sendcount:', self.sendCount, self.url);
			}
			
			if(self.method=='comet' && self.paths2ignore.indexOf(self.path)<0) self.wsloopHandler=$.ajax({
				type: 'POST',
				url: self.urlprefix+'wsloop.php',     
				data: {key: 'FalseWebSocket', ui: self.UI, messagecount: self.messageCount, sendcount: self.sendCount, path:self.path, replsuff:self.repl_suff},
				dataType: 'text',
				cache: false,
				timeout: 0,
				success: function(data) {
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'wsloop finished', data, self.url);
					}
					var adata= data.split(':');
					if(adata[0]=='error' || data=="") {
						self.wsLoopReconn++;
						if(data!="error:stopped" && self.wsLoopReconn<self.maxWsReconn && self.readyState != WebSocket.CLOSED) {
							self.open(true);
						}
						else {
							// self.readyState = WebSocket.CLOSED;
							// self.close()
							adata.shift();
							generateError(adata.length>0?adata[0]:'error', adata.join(':'));
						}
					}
					else {
						// self.readyState = WebSocket.CLOSED;
						// eventTarget.dispatchEvent(generateEvent('close'));
						return;
					}
					
				},
				error: function(jqXHR, textStatus, error){
					
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'wsloop error:'+textStatus+'|'+error, 'wsLoopReconn:'+self.wsLoopReconn+' of '+self.maxWsReconn, self.url);
					}
					self.wsLoopReconn++;
					if(textStatus!="abort" && self.wsLoopReconn<self.maxWsReconn && self.readyState != WebSocket.CLOSED) {
						self.open(true);
					}
					else {
						// self.readyState = WebSocket.CLOSED;
						// self.close();
						if(textStatus!="abort") generateError(textStatus, error);
					}
					
					//console.log("-connecting error:"+textStatus+"|"+error);
				}
			});
			if(self.readyState != WebSocket.OPEN) {
				var e = generateEvent('open');
				e.isReconnect = reconnectAttempt;
				eventTarget.dispatchEvent(e);
			}
			self.readyState = WebSocket.OPEN;
			
			if(self.method=='comet') self.waitForMessages();
			else {
				clearInterval(self.findMessageInterval);
				if(self.paths2ignore.indexOf(self.path)<0 || self.sendCount>0) self.findMessageInterval=setInterval(function(){self.messageFinderCount++; self.messageFinder(self.messageFinderCount);},self.messageFinderInterval);
			}
			if(self.paths2ignore.indexOf(self.path)<0 || self.sendCount>0){
				clearInterval(self.confirmInterval);
				self.confirmInterval=setInterval(self.confirmFalseWebSocket,self.confirmDelay);
			}
		}
		
        this.waitForMessages = function () {
			if(self.paths2ignore.indexOf(self.path)<0 || self.sendCount>0) self.msloopHandler=$.ajax({
				type: 'POST',
				url: self.urlprefix+'messageloop.php',     
				data: {key: 'FalseWebSocket', ui: self.UI, messagecount: self.messageCount, sendcount: self.sendCount},
				dataType: 'text',
				cache: false,
				timeout: 0,
				success: function(resp) {
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('MessageReceived `'+resp+'`', self.url);
					}
					if(resp === '' || resp === null) {
						self.waitForMessages();
						return;
					}
					if(resp == 'error:stopped') {
						self.readyState = WebSocket.CLOSED;
						return;
					}
					var conn_closed_event=false;
					var jresp={};
					if(testJSON(resp)) {
						jresp = JSON.parse(resp);
						if(typeof jresp == 'object' && 'conn_closed' in jresp && jresp['conn_closed']) conn_closed_event=true;
					}
					if(conn_closed_event) {
						if(!('reason' in jresp) || (jresp['reason']!='timeout' && jresp['reason']!='confirmtimeout' && jresp['reason'].split(':')[0]!='error')) {
							// self.readyState = WebSocket.CLOSED;
							return;
						}
						else {
							self.wsMessReconn++;
							if(self.wsMessReconn<self.maxMessReconn && self.readyState != WebSocket.CLOSED) {
								self.waitForMessages();
							}
							else {
								// self.close();
								generateError(('reason' in jresp)?jresp['reason']:"unknown", ('reason' in jresp)?jresp['reason']:"unknown");
							}
						}
					}
					else {
						var e = generateEvent('message');
						// for(var rk in resp) e[rk]=resp[rk];
						e["origin"]=resp;
						e["lastEventId"]=self.messageCount.toString();
						// e["source"]="";
						// e["ports"]="";
						e["data"]=resp;
						self.messageCount++;
						eventTarget.dispatchEvent(e);
						self.wsMessReconn=0;
						if(self.readyState != WebSocket.CLOSED)
							self.waitForMessages();
					}
				},
				error: function(jqXHR, textStatus, error){
					
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'messageloop error:'+textStatus+'|'+error, 'wsMessReconn:'+self.wsLoopReconn+' of '+self.maxWsReconn, self.url);
					}
					self.wsMessReconn++;
					if(textStatus!="abort" && self.wsMessReconn<self.maxMessReconn && self.readyState != WebSocket.CLOSED) {
						self.waitForMessages();
					}
					else {
						if(textStatus!="abort") generateError(textStatus, error);
						// self.close();
					}
					
					//console.log("-connecting error:"+textStatus+"|"+error);
				}
			});
			if (self.debug || FalseWebSocket.debugAll) {
				console.debug('waitForMessages started with UI='+self.UI+', messagecount='+self.messageCount, self.url);
			}
        }
		
        this.messageFinder = function (gencount) {
			self.messageFinderStatuses[gencount]="1";
			self.messageFinderHandlers[gencount]=$.ajax({
				type: 'POST',
				url: self.urlprefix+'messagefinder.php',     
				data: {key: 'FalseWebSocket', ui: self.UI, messagecount: self.messageCount, sendcount: self.sendCount},
				dataType: 'text',
				cache: false,
				timeout: 0,
				success: function(resp) {
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('MessageReceived `'+resp+'`', self.url);
					}
					self.messageFinderStatuses[gencount]=0;
					self.messageFoundCount++;
					if(resp === '' || resp === null) {
						return;
					}
					if(resp == 'error:stopped') {
						self.readyState = WebSocket.CLOSED;
						clearInterval(self.findMessageInterval);
						generateError(resp,"Experiment stopped");
						return;
					}
					for(var h in self.messageFinderHandlers) if(h<gencount && self.messageFinderStatuses[h]>0) {
						try {
							self.messageFinderHandlers[h].abort();
						} catch(error) {
							if (self.debug || FalseWebSocket.debugAll) {
								console.debug('FalseWebSocket', 'error aborting messageFinderLoop #'+h+ " success function "+gencount, error, "messageFoundCount="+self.messageFoundCount);
							}		
						}
					}
					if(resp == 'wait') return;
					var conn_closed_event=false;
					var jresp={};
					if(testJSON(resp)) {
						jresp = JSON.parse(resp);
						if(typeof jresp == 'object' && 'conn_closed' in jresp && jresp['conn_closed']) conn_closed_event=true;
					}
					if(conn_closed_event) {
						if(!('reason' in jresp) || (jresp['reason']!='timeout' && jresp['reason']!='confirmtimeout' && jresp['reason'].split(':')[0]!='error')) {
							// self.readyState = WebSocket.CLOSED;
							return;
						}
						else {
							generateError(('reason' in jresp)?jresp['reason']:"unknown", ('reason' in jresp)?jresp['reason']:"unknown");
						}
					}
					else {
						var e = generateEvent('message');
						// for(var rk in resp) e[rk]=resp[rk];
						e["origin"]=resp;
						e["lastEventId"]=self.messageCount.toString();
						// e["source"]="";
						// e["ports"]="";
						e["data"]=resp;
						self.messageCount++;
						eventTarget.dispatchEvent(e);
						self.wsMessReconn=0;
					}
				},
				error: function(jqXHR, textStatus, error){
					
					self.messageFinderStatuses[gencount]=-1;
					self.wsMessReconn++;
					if(textStatus!="abort" && self.readyState != WebSocket.CLOSED) {
						if (self.debug || FalseWebSocket.debugAll) {
							console.debug('FalseWebSocket', 'messageFinderError error:'+textStatus+'|'+error, 'wsMessReconn:'+self.wsLoopReconn+' of '+self.maxWsReconn, self.url);
						}
					}
					else if(self.wsMessReconn>self.maxMessErrors) {
						if(textStatus!="abort") generateError(textStatus, error);
						// self.close();
					}
					
					//console.log("-connecting error:"+textStatus+"|"+error);
				}
			});
        }
		
		this.confirmFalseWebSocket =function(type) {
			if(type === undefined) type="time"
			if (self.debug || FalseWebSocket.debugAll) {
				var d= new Date();
				console.debug('FalseWebSocket', 'sending confirmation of type '+type, d, self.UI);
			}
			$.ajax({
				type: 'POST',
				url: self.urlprefix+'confirmfwsocket.php',
				data: {key: 'FalseWebSocket', ui: self.UI, type:type},
				dataType: 'text',
				cache: false,
				timeout:this.confirmDelay,
				success: function(data) {
					if(data!="Your are not authorized.") {
						if (self.debug || FalseWebSocket.debugAll) {
							// console.debug('FalseWebSocket', 'confirmation sent. UI='+data, self.url);
						}
					}
					else {
						if (self.debug || FalseWebSocket.debugAll) {
							console.debug('FalseWebSocket', 'connecting error: bad key', self.url);
						}
						// self.readyState = WebSocket.CLOSED;
						generateError("BadKey", "Bad key provided for gethandler.php");
					}
				},
				error: function(jqXHR, textStatus, error) {
					// clearTimeout(timeout);
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'confirm error:'+textStatus+'|'+error, self.url);
					}
				}
			});
		}

        // Whether or not to create a websocket upon instantiation
        if (this.automaticOpen == true) {
            this.connect(false);
        }

        /**
         * Transmits data to the server over the WebSocket connection.
         *
         * @param data a text string, ArrayBuffer or Blob to send to the server.
         */
        this.send = function(data) {
			if(self.paths2ignore.indexOf(self.path)>=0 && self.sendCount<=0) self.open(true);
			$.ajax({
				type: 'POST',
				url: self.urlprefix+'sendmessage.php',     
				data: {key: 'FalseWebSocket', ui: self.UI, messagecount: self.messageCount, sendcount: self.sendCount, data:data, unignore:(self.method!='comet' && self.paths2ignore.indexOf(self.path)>=0 && self.sendCount<=0)?"1":"0"},
				dataType: 'text',
				cache: false,
				timeout: 10000,
				success: function(resp) {
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('Send Message returned:', resp, self.url);
					}
					if(resp!="OK") {
						self.sendingAttempt++;
						if(self.sendingAttempt<this.maxSendAttempts && self.readyState != WebSocket.CLOSED) {
							self.send(data);
						}
						else {
							// self.close();
							generateError("Send Error", "File Writting problem:"+resp);
						}
					}
					else self.sendingAttempt=0;
				},
				error: function(jqXHR, textStatus, error){
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'send message error:'+textStatus+'|'+error, self.url);
					}
					self.sendingAttempt++;
					if(self.sendingAttempt<this.maxSendAttempts && self.readyState != WebSocket.CLOSED) {
						self.send(data);
					}
					else {
						// self.readyState = WebSocket.CLOSED;
						// self.close();
						generateError(textStatus, error);
					}
				}
			});
			if (self.debug || FalseWebSocket.debugAll) {
				console.debug('sending message '+data+' with UI='+self.UI+', messagecount='+self.messageCount+', sendcount='+self.sendCount, self.url);
			}
			self.sendCount++;
			return (self.readyState != WebSocket.CLOSED);
        };
		
		this.clearPendingTasks = function()
		{
			if (self.debug || FalseWebSocket.debugAll) {
				console.debug('clearing pending tasks, UI='+self.UI);
			}
			clearInterval(self.confirmInterval);
			if(self.method=='comet') {
				try {
					self.wsloopHandler.abort();
				} catch(error) {
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'error aborting wsloop', error);
					}				
				}
				try {
					self.msloopHandler.abort();
				} catch(error) {
					if (self.debug || FalseWebSocket.debugAll) {
						console.debug('FalseWebSocket', 'error aborting messageloop', error);
					}				
				}
			}
			else {
				clearInterval(self.findMessageInterval);
				for(var h in self.messageFinderHandlers) if(self.messageFinderStatuses[h]>0) {
					try {
						self.messageFinderHandlers[h].abort();
					} catch(error) {
						if (self.debug || FalseWebSocket.debugAll) {
							console.debug('FalseWebSocket', 'error aborting messageFinderLoop #'+h, error);
						}				
					}
				}
				self.messageFinderCount=0; self.messageFoundCount=0; self.messageFinderHandlers={}; self.messageFinderStatuses={};
			}
			self.UI=0;
			
		}

        /**
         * Closes the WebSocket connection or connection attempt, if any.
         * If the connection is already CLOSED, this method does nothing.
         */
        this.close = function(code, reason) {
            // Default CLOSE_NORMAL code
			if (self.debug || FalseWebSocket.debugAll) {
				console.debug('FalseWebSocket', 'close function called, UI='+self.UI, code, reason, self.url);
			}
			if (self.UI==0) return;
			self.clearPendingTasks();
            if (typeof code == 'undefined') {
                code = 1000;
            }
            if (typeof reason == 'undefined') {
                reason = '';
            }
			forcedClose = true;
            if(self.readyState != WebSocket.CLOSED) {
				if (self.debug || FalseWebSocket.debugAll) {
					console.debug('FalseWebSocket', '!Closing FalseWebSocket!', code, reason, self.url);
				}
				// this.send("!close!");
				self.confirmFalseWebSocket("finished");
				eventTarget.dispatchEvent(generateEvent('close'));
			}
			self.readyState = WebSocket.CLOSED;
        };
    }

    /**
     * An event listener to be called when the WebSocket connection's readyState changes to OPEN;
     * this indicates that the connection is ready to send and receive data.
     */
    FalseWebSocket.prototype.onopen = function(event) {};
    /** An event listener to be called when the WebSocket connection's readyState changes to CLOSED. */
    FalseWebSocket.prototype.onclose = function(event) {};
    /** An event listener to be called when a connection begins being attempted. */
    FalseWebSocket.prototype.onconnecting = function(event) {};
    /** An event listener to be called when a message is received from the server. */
    FalseWebSocket.prototype.onmessage = function(event) {};
    /** An event listener to be called when an error occurs. */
    FalseWebSocket.prototype.onerror = function(event) {};

    /**
     * Whether all instances of FalseWebSocket should log debug messages.
     * Setting this to true is the equivalent of setting all instances of FalseWebSocket.debug to true.
     */
    FalseWebSocket.debugAll = false;

    FalseWebSocket.CONNECTING = WebSocket.CONNECTING;
    FalseWebSocket.OPEN = WebSocket.OPEN;
    FalseWebSocket.CLOSING = WebSocket.CLOSING;
    FalseWebSocket.CLOSED = WebSocket.CLOSED;

    return FalseWebSocket;
});
