<?php
require_once("fwshelper.php");
if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket") exit("Your are not authorized.");

$id = (!empty($_SERVER["UNIQUE_ID"]))?$_SERVER["UNIQUE_ID"]:uniqid("",true);
echo str_replace(array('\\','/',':','*','?','"','<','>','|', ' ', "'"),'',$id);
define("CLEAN",true);
include("cleanwsdata.php");
?>