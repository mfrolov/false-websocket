<?php
if(!defined("CLEAN")) exit("Your are not authorized.");
if(!CLEAN) exit;
require_once("fwshelper.php");
function cleandata($preftype="confirm",$action="check", $id="") {
	if(!is_array($preftype)) $typefile=explode(",",$preftype);
	else $typefile=$preftype;
	$wsfolder=empty($config["wsdata_path"])?"wsdata":$config["wsdata_path"];
	$wsfiles=scandir($wsfolder);
	$ctime=microtime(true);
	foreach($wsfiles as $wsf)
	{
		$af=explode("_",$wsf);
		if(count($af)>1 && in_array($af[0],$typefile) && (empty($id) || $id==$af[1])) {
			$wsfpath=$wsfolder."/".$wsf;
			$filetime=@filemtime($wsfpath);
			if($filetime!==false && $ctime-$filetime>60) {
				if($action=="check") {
					$cont=getNewLine($wsfpath);
					if($cont=="finished" || ($cont!==false && $ctime-$filetime>600)) {
						cleandata("mess,send,confirm","delete",$af[1]);
					}
				}
				if($action=="delete") {
					$deleted=@unlink($wsfpath);
				}
			}
		}
	}
}
cleandata();
?>