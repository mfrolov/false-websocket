<?php
  $protocol = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off' ? 'https' : 'http';
  header('Location: '.$protocol.'://'.$_SERVER['SERVER_NAME']);
  exit;
?>